DROP table IF EXISTS empleado;

create table empleado(
	id int auto_increment PRIMARY KEY,
	nombre varchar(250),
	apellido varchar(250),
	trabajo enum('periodista', 'informàtic', 'enginyer'),
	salario decimal(5)
);

insert into empleado (nombre, apellido, trabajo, salario)values('Jaume','Cartoixa','periodista',1850.0);
insert into empleado (nombre, apellido, trabajo, salario)values('Mariona','Roca','enginyer',2100.0);
insert into empleado (nombre, apellido, trabajo, salario)values('Raquel','Balada','informàtic',1950.0);
insert into empleado (nombre, apellido, trabajo, salario)values('Nàdia','Carles','enginyer',2100.0);
insert into empleado (nombre, apellido, trabajo, salario)values('Sandra','Vidal','informàtic',1950.0);