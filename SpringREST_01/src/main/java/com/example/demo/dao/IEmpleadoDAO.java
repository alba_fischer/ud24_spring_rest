package com.example.demo.dao;


import org.springframework.data.jpa.repository.JpaRepository;
import com.example.demo.dto.Empleado;
import java.util.List;

public interface IEmpleadoDAO extends JpaRepository<Empleado, Long>{
	public List<Empleado> findByTrabajo(String trabajo);
}

